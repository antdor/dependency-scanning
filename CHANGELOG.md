# GitLab Dependency Scanning

GitLab Dependency Scanning follows versioning of GitLab (`MAJOR.MINOR` only) and generates a `MAJOR-MINOR-stable` [Docker image](https://gitlab.com/gitlab-org/security-products/dependency-scanning/container_registry).

These "stable" Docker images may be updated after release date, changes are added to the corresponding section bellow.

## 10-7-stable
- Initial release

## 10-6-stable
- **Backport:** Initial release

## 10-5-stable
- **Backport:** Initial release
